import { Global, Module } from '@nestjs/common';
import { EmailAccountService } from './email-account/email-account.service';
import { EmailModuleEntities } from './entities';

@Global()
@Module({
  providers: [...EmailModuleEntities, EmailAccountService],
  exports: [...EmailModuleEntities, EmailAccountService],
})
export class EmailEntitiesModule {}
