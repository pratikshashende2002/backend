import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

const schema = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4 },
    name: String,
    host: String,
    user: String,
    from: String,
    pass: String,
    secure: Boolean,
    port: Number,
    pool: Boolean,
    tlsRejectUnauthorized: Boolean,
    oauth2: Boolean, // set to true for OAuth2 Accounts
    owner: String, // uuid of owner user
    sharedWithUsers: [String],
  },
  { collection: 'email_account', versionKey: false },
);

export const EmailAccount = schema;

export const EMAIL_ACCOUNT = 'EmailAccount';

export const EmailAccountModel = mongoose.model(EMAIL_ACCOUNT, EmailAccount);
