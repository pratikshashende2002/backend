import {
  IsUrl,
  IsNumberString,
  IsString,
  IsEmail,
  IsOptional,
  IsBoolean,
} from 'class-validator';

export class CreateEmailDto {
  uuid?: string;
  owner?: string;

  @IsString()
  name: string;

  @IsUrl({ require_protocol: false, require_tld: false })
  host: string;

  @IsNumberString()
  port: number;

  @IsString()
  @IsOptional()
  user: string;

  @IsString()
  @IsOptional()
  pass: string;

  @IsEmail()
  from: string;

  @IsOptional()
  @IsBoolean()
  secure?: boolean;
}
