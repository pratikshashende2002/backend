import { join } from 'path';

export const VIEWS_DIR = join(process.cwd(), '/src/views');
export const ACCOUNTS_ROUTE = '/account'; // html5 route
export const ACCOUNT_CHOOSE_ROUTE = '/account/choose';
export const LOGIN_ROUTE = '/login';
export const FORGOT_ROUTE = '/forgot';
export const SIGNUP_ROUTE = '/signup';
export const VERIFY_ROUTE = '/verify';
export const ADMINISTRATOR = 'administrator';
export const ADMIN_ROLE = 'admin';
export const ADMINISTRATOR_NAME = 'Administrator';
export const ADMIN_EMAIL = 'Administrator';
export const ROLES = 'roles';
export const AUTHORIZATION = 'authorization';
export const SCOPE_OPENID = 'openid';
export const SCOPE_EMAIL = 'email';
export const SCOPE_ROLES = 'roles';
export const SCOPE_PROFILE = 'profile';
export const SCOPE_PHONE = 'phone';
export const SWAGGER_ROUTE = 'api-docs';
export const SUCCESS = 'Success';
export const THIRTY_NUMBER = 30;
export const TEN_NUMBER = 10;
export const SERVICE = 'authorization-server';
export const INFRASTRUCTURE_CONSOLE = 'Infrastructure Console';
export const CALLBACK_ROUTE = '/callback';
export const API = 'api';
export const SELECT_ACCOUNT = 'select_account';
export const LOGIN = 'login';
export const NONE = 'none';
export const TRUE = 'true';
export const WWW_NEGOTIATE_HEADER_KEY = 'WWW-Authenticate';
export const NEGOTIATE = 'Negotiate';
export const SEND_EMAIL = 'send_email';
export const TOKEN = 'token';
export const CHANNEL = 'com_channel';
export const ACL_PUBLIC_PERMISSION = 'public-read';
export const PUBLIC = 'public';
export const APP_NAME = SERVICE;
