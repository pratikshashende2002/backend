import {
  IsString,
  IsUrl,
  IsNotEmpty,
  IsOptional,
  IsArray,
  IsUUID,
} from 'class-validator';

export class OAuth2ProviderDto {
  @IsUUID()
  @IsOptional()
  uuid: string;

  @IsString()
  @IsNotEmpty()
  name: string;

  @IsUrl({ require_tld: false })
  authServerURL: string;

  @IsString()
  @IsNotEmpty()
  clientId: string;

  @IsString()
  @IsNotEmpty()
  clientSecret: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  profileURL: string;

  @IsUrl({ require_tld: false })
  tokenURL: string;

  @IsUrl({ require_tld: false })
  authorizationURL: string;

  @IsUrl({ require_tld: false })
  revocationURL: string;

  @IsArray()
  @IsNotEmpty({ each: true })
  scope: string[];
}
