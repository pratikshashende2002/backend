import { Test, TestingModule } from '@nestjs/testing';
import { OAuth2TokenService } from './oauth2-token.service';
import { OAUTH2_TOKEN } from './oauth2-token.schema';

describe('Oauth2TokenService', () => {
  let service: OAuth2TokenService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OAuth2TokenService, { provide: OAUTH2_TOKEN, useValue: {} }],
    }).compile();

    service = module.get<OAuth2TokenService>(OAuth2TokenService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
