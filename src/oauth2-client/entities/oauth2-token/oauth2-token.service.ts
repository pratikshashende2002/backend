import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { OAUTH2_TOKEN } from './oauth2-token.schema';
import { OAuth2Token } from './oauth2-token.interface';

@Injectable()
export class OAuth2TokenService {
  constructor(
    @Inject(OAUTH2_TOKEN)
    private readonly model: Model<OAuth2Token>,
  ) {}

  async save(params) {
    return await new this.model(params).save();
  }

  async find(params?): Promise<OAuth2Token[]> {
    return await this.model.find(params);
  }

  async findOne(params) {
    return await this.model.findOne(params);
  }

  async update(query, params) {
    const doc = await this.findOne(query);
    if (doc) {
      Object.assign(doc, params);
      return await doc.save();
    }
  }
}
