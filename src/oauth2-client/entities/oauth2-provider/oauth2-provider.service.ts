import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { OAUTH2_PROVIDER } from './oauth2-provider.schema';
import { OAuth2Provider } from './oauth2-provider.interface';

@Injectable()
export class OAuth2ProviderService {
  constructor(
    @Inject(OAUTH2_PROVIDER)
    private readonly model: Model<OAuth2Provider>,
  ) {}

  async save(params) {
    return await new this.model(params).save();
  }

  async find(params?): Promise<OAuth2Provider[]> {
    return await this.model.find(params);
  }

  async findOne(params) {
    return await this.model.findOne(params);
  }

  async update(query, params) {
    const doc = await this.findOne(query);
    if (doc) {
      Object.assign(doc, params);
      return await doc.updateOne();
    }
  }

  async count() {
    return await this.model.estimatedDocumentCount();
  }

  async list(
    offset: number,
    limit: number,
    search: string,
    query: any,
    sortQuery?: any,
  ) {
    if (search) {
      // Search through multiple keys
      // https://stackoverflow.com/a/41390870
      const nameExp = new RegExp(search, 'i');
      query.$or = ['name', 'uuid'].map(field => {
        const out = {};
        out[field] = nameExp;
        return out;
      });
    }

    const data = this.model
      .find(query)
      .skip(Number(offset))
      .limit(Number(limit))
      .sort(sortQuery);

    return {
      docs: await data.exec(),
      length: await this.model.countDocuments(query),
      offset: Number(offset),
    };
  }

  createNew(params?) {
    return new this.model(params);
  }
}
