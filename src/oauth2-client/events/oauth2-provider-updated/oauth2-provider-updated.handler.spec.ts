import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { OAuth2ProviderUpdatedHandler } from './oauth2-provider-updated.handler';
import { OAuth2ProviderUpdatedEvent } from './oauth2-provider-updated.event';
import { OAuth2Provider } from '../../entities/oauth2-provider/oauth2-provider.interface';
import { OAuth2ProviderModel } from '../../entities/oauth2-provider/oauth2-provider.schema';

describe('Event: OAuth2ProviderUpdatedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: OAuth2ProviderUpdatedHandler;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        OAuth2ProviderUpdatedHandler,
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<OAuth2ProviderUpdatedHandler>(
      OAuth2ProviderUpdatedHandler,
    );
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should save OAuth2Provider', async () => {
    eventBus$.publish = jest.fn(() => {});
    const mockProvider = {} as OAuth2Provider;
    mockProvider.updateOne = jest.fn(() =>
      new OAuth2ProviderModel().deleteOne(),
    );
    await eventHandler.handle(new OAuth2ProviderUpdatedEvent(mockProvider));
    expect(mockProvider.updateOne).toHaveBeenCalledTimes(1);
  });
});
