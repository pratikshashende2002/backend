import { Test, TestingModule } from '@nestjs/testing';
import { LDAPClientAggregateService } from './ldap-client-aggregate.service';
import { UserManagementService } from '../../../user-management/aggregates/user-management/user-management.service';
import { LDAPClientService } from '../../entities/ldap-client/ldap-client.service';
import { KerberosRealmService } from '../../entities/kerberos-realm/kerberos-realm.service';

describe('LDAPAggregateService', () => {
  let service: LDAPClientAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        LDAPClientAggregateService,
        { provide: LDAPClientService, useValue: {} },
        { provide: KerberosRealmService, useValue: {} },
        { provide: UserManagementService, useValue: {} },
      ],
    }).compile();

    service = module.get<LDAPClientAggregateService>(
      LDAPClientAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
