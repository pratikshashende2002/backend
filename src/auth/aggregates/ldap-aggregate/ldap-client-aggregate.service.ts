import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { createClient, filters, ClientOptions } from 'ldapjs';
import { v4 as uuidv4 } from 'uuid';

import { User } from '../../../user-management/entities/user/user.interface';
import { UserManagementService } from '../../../user-management/aggregates/user-management/user-management.service';
import { UserAccountAddedEvent } from '../../../user-management/events/user-account-added/user-account-added.event';
import { LDAPClientService } from '../../entities/ldap-client/ldap-client.service';
import { LDAPClient } from '../../entities/ldap-client/ldap-client.interface';
import { CreateLDAPClientDto } from '../../controllers/ldap-client/ldap-client-create.dto';
import { LDAPClientAddedEvent } from '../../events/ldap-client-added/ldap-client-added.event';
import { LDAPClientRemovedEvent } from '../../events/ldap-client-removed/ldap-client-removed.event';
import { LDAPClientModifiedEvent } from '../../events/ldap-client-modified/ldap-client-modified.event';
import { KerberosRealmService } from '../../entities/kerberos-realm/kerberos-realm.service';

export interface LDAPUser {
  dn: string;
  [key: string]: string | string[] | undefined;
}

export interface SearchResultEntry {
  type: string;
  objectName: string;
  attributes: {
    type: string;
    values: string[];
  }[];
}

export class LdapAuthenticationError extends Error {
  constructor(message) {
    super(message);
    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);
  }
}

export interface AuthenticationOptions {
  ldapOpts: ClientOptions;
  userDn?: string;
  adminDn?: string;
  adminPassword?: string;
  userSearchBase?: string;
  usernameAttribute?: string;
  username?: string;
  verifyUserExists?: boolean;
  starttls?: boolean;
  groupsSearchBase?: string;
  groupClass?: string;
  groupMemberAttribute?: string;
  groupMemberUserAttribute?: string;
  userPassword?: string;
  attributes?: string[];
}

@Injectable()
export class LDAPClientAggregateService extends AggregateRoot {
  constructor(
    private readonly userManager: UserManagementService,
    private readonly ldap: LDAPClientService,
    private readonly kerberosRealm: KerberosRealmService,
  ) {
    super();
  }

  searchResultToUser(pojo: SearchResultEntry) {
    const user = { dn: pojo.objectName };
    pojo.attributes.forEach(attribute => {
      user[attribute.type] =
        attribute.values.length === 1 ? attribute.values[0] : attribute.values;
    });
    return user;
  }

  ldapBind(
    dn: string,
    password: string,
    starttls: boolean,
    ldapOpts: ClientOptions,
  ) {
    return new Promise(function (resolve, reject) {
      ldapOpts.connectTimeout = ldapOpts.connectTimeout || 5000;
      const client = createClient(ldapOpts);

      client.on('connect', function () {
        if (starttls) {
          client.starttls(ldapOpts.tlsOptions, null, function (error) {
            if (error) {
              reject(error);
              return;
            }
            client.bind(dn, password, function (err) {
              if (err) {
                reject(err);
                client.unbind();
                return;
              }
              ldapOpts.log && ldapOpts.log.trace('bind success!');
              resolve(client);
            });
          });
        } else {
          client.bind(dn, password, function (err) {
            if (err) {
              reject(err);
              client.unbind();
              return;
            }
            ldapOpts.log && ldapOpts.log.trace('bind success!');
            resolve(client);
          });
        }
      });

      client.on('timeout', err => {
        reject(err);
      });
      client.on('connectTimeout', err => {
        reject(err);
      });
      client.on('error', err => {
        reject(err);
      });

      client.on('connectError', function (error) {
        if (error) {
          reject(error);
          return;
        }
      });
    });
  }

  searchUser(
    ldapClient,
    searchBase,
    usernameAttribute,
    username,
    attributes = null,
  ): Promise<LDAPUser> {
    const self = this;
    return new Promise(function (resolve, reject) {
      const filter = new filters.EqualityFilter({
        attribute: usernameAttribute,
        value: username,
      });
      const searchOptions = {
        filter,
        scope: 'sub',
        attributes,
      };
      if (attributes) {
        searchOptions.attributes = attributes;
      }
      ldapClient.search(searchBase, searchOptions, function (err, res) {
        let user = null;
        if (err) {
          reject(err);
          ldapClient.unbind();
          return;
        }
        res.on('searchEntry', function (entry) {
          user = self.searchResultToUser(entry.pojo);
        });
        res.on('searchReference', function (referral) {
          // TODO: we don't support reference yet
          // If the server was able to locate the entry referred to by the baseObject
          // but could not search one or more non-local entries,
          // the server may return one or more SearchResultReference messages,
          // each containing a reference to another set of servers for continuing the operation.
          // referral.uris
        });
        res.on('error', function (err) {
          reject(err);
          ldapClient.unbind();
        });
        res.on('end', function (result) {
          if (result.status !== 0) {
            reject(new Error('ldap search status is not 0, search failed'));
          } else {
            resolve(user);
          }
          ldapClient.unbind();
        });
      });
    });
  }

  searchUserGroups(
    ldapClient,
    searchBase,
    user,
    groupClass,
    groupMemberAttribute = 'member',
    groupMemberUserAttribute = 'dn',
  ): Promise<string[]> {
    return new Promise(function (resolve, reject) {
      ldapClient.search(
        searchBase,
        {
          filter: `(&(objectclass=${groupClass})(${groupMemberAttribute}=${user[groupMemberUserAttribute]}))`,
          scope: 'sub',
        },
        function (err, res) {
          const groups = [];
          if (err) {
            reject(err);
            ldapClient.unbind();
            return;
          }
          res.on('searchEntry', function (entry) {
            groups.push(entry.pojo);
          });
          res.on('searchReference', function (referral) {});
          res.on('error', function (err) {
            reject(err);
            ldapClient.unbind();
          });
          res.on('end', function (result) {
            if (result.status !== 0) {
              reject(new Error('ldap search status is not 0, search failed'));
            } else {
              resolve(groups);
            }
            ldapClient.unbind();
          });
        },
      );
    });
  }

  async authenticateWithAdmin(
    adminDn,
    adminPassword,
    userSearchBase,
    usernameAttribute,
    username,
    userPassword,
    starttls,
    ldapOpts,
    groupsSearchBase,
    groupClass,
    groupMemberAttribute = 'member',
    groupMemberUserAttribute = 'dn',
    attributes = null,
  ) {
    let ldapAdminClient;
    try {
      ldapAdminClient = await this.ldapBind(
        adminDn,
        adminPassword,
        starttls,
        ldapOpts,
      );
    } catch (error) {
      throw new LdapAuthenticationError({ admin: error });
    }
    const user = await this.searchUser(
      ldapAdminClient,
      userSearchBase,
      usernameAttribute,
      username,
      attributes,
    );
    ldapAdminClient.unbind();
    if (!user || !user.dn) {
      ldapOpts.log &&
        ldapOpts.log.trace(
          `admin did not find user! (${usernameAttribute}=${username})`,
        );
      throw new LdapAuthenticationError(
        'user not found or usernameAttribute is wrong',
      );
    }
    const userDn = user.dn;
    let ldapUserClient;
    try {
      ldapUserClient = await this.ldapBind(
        userDn,
        userPassword,
        starttls,
        ldapOpts,
      );
    } catch (error) {
      throw new LdapAuthenticationError(error);
    }
    ldapUserClient.unbind();
    if (groupsSearchBase && groupClass && groupMemberAttribute) {
      try {
        ldapAdminClient = await this.ldapBind(
          adminDn,
          adminPassword,
          starttls,
          ldapOpts,
        );
      } catch (error) {
        throw error;
      }
      const groups = await this.searchUserGroups(
        ldapAdminClient,
        groupsSearchBase,
        user,
        groupClass,
        groupMemberAttribute,
        groupMemberUserAttribute,
      );
      user.groups = groups;
      ldapAdminClient.unbind();
    }
    return user;
  }

  async authenticateWithUser(
    userDn,
    userSearchBase,
    usernameAttribute,
    username,
    userPassword,
    starttls,
    ldapOpts,
    groupsSearchBase,
    groupClass,
    groupMemberAttribute = 'member',
    groupMemberUserAttribute = 'dn',
    attributes = null,
  ) {
    let ldapUserClient;
    try {
      ldapUserClient = await this.ldapBind(
        userDn,
        userPassword,
        starttls,
        ldapOpts,
      );
    } catch (error) {
      throw error;
    }
    if (!usernameAttribute || !userSearchBase) {
      // if usernameAttribute is not provided, no user detail is needed.
      ldapUserClient.unbind();
      return true;
    }
    const user = await this.searchUser(
      ldapUserClient,
      userSearchBase,
      usernameAttribute,
      username,
      attributes,
    );
    if (!user || !user.dn) {
      ldapOpts.log &&
        ldapOpts.log.trace(
          `user logged in, but user details could not be found. (${usernameAttribute}=${username}). Probabaly wrong attribute or searchBase?`,
        );
      throw new LdapAuthenticationError(
        'user logged in, but user details could not be found. Probabaly usernameAttribute or userSearchBase is wrong?',
      );
    }
    ldapUserClient.unbind();
    if (groupsSearchBase && groupClass && groupMemberAttribute) {
      try {
        ldapUserClient = await this.ldapBind(
          userDn,
          userPassword,
          starttls,
          ldapOpts,
        );
      } catch (error) {
        throw error;
      }
      const groups = await this.searchUserGroups(
        ldapUserClient,
        groupsSearchBase,
        user,
        groupClass,
        groupMemberAttribute,
        groupMemberUserAttribute,
      );
      user.groups = groups;
      ldapUserClient.unbind();
    }
    return user;
  }

  async verifyUserExists(
    adminDn,
    adminPassword,
    userSearchBase,
    usernameAttribute,
    username,
    starttls,
    ldapOpts,
    groupsSearchBase,
    groupClass,
    groupMemberAttribute = 'member',
    groupMemberUserAttribute = 'dn',
    attributes = null,
  ) {
    let ldapAdminClient;
    try {
      ldapAdminClient = await this.ldapBind(
        adminDn,
        adminPassword,
        starttls,
        ldapOpts,
      );
    } catch (error) {
      throw new LdapAuthenticationError({ admin: error });
    }
    const user = await this.searchUser(
      ldapAdminClient,
      userSearchBase,
      usernameAttribute,
      username,
      attributes,
    );
    ldapAdminClient.unbind();
    if (!user || !user.dn) {
      ldapOpts.log &&
        ldapOpts.log.trace(
          `admin did not find user! (${usernameAttribute}=${username})`,
        );
      throw new LdapAuthenticationError(
        'user not found or usernameAttribute is wrong',
      );
    }
    if (groupsSearchBase && groupClass && groupMemberAttribute) {
      try {
        ldapAdminClient = await this.ldapBind(
          adminDn,
          adminPassword,
          starttls,
          ldapOpts,
        );
      } catch (error) {
        throw error;
      }
      const groups = await this.searchUserGroups(
        ldapAdminClient,
        groupsSearchBase,
        user,
        groupClass,
        groupMemberAttribute,
        groupMemberUserAttribute,
      );
      user.groups = groups;
      ldapAdminClient.unbind();
    }
    return user;
  }

  async authenticate(options: AuthenticationOptions) {
    if (!options.userDn) {
      if (!options.adminDn) {
        throw new LdapAuthenticationError(
          'Admin mode adminDn must be provided',
        );
      }
      if (!options.adminPassword) {
        throw new LdapAuthenticationError(
          'Admin mode adminPassword must be provided',
        );
      }
      if (!options.userSearchBase) {
        throw new LdapAuthenticationError(
          'Admin mode userSearchBase must be provided',
        );
      }
      if (!options.usernameAttribute) {
        throw new LdapAuthenticationError(
          'Admin mode usernameAttribute must be provided',
        );
      }
      if (!options.username) {
        throw new LdapAuthenticationError(
          'Admin mode username must be provided',
        );
      }
    }
    if (!options.ldapOpts && options.ldapOpts.url) {
      throw new LdapAuthenticationError('ldapOpts.url must be provided');
    }
    if (options.verifyUserExists) {
      if (!options.adminDn) {
        throw new LdapAuthenticationError(
          'Admin mode adminDn must be provided',
        );
      }
      if (!options.adminPassword) {
        throw new LdapAuthenticationError(
          'adminDn and adminPassword must be both provided.',
        );
      }
      return await this.verifyUserExists(
        options.adminDn,
        options.adminPassword,
        options.userSearchBase,
        options.usernameAttribute,
        options.username,
        options.starttls,
        options.ldapOpts,
        options.groupsSearchBase,
        options.groupClass,
        options.groupMemberAttribute,
        options.groupMemberUserAttribute,
      );
    }
    if (!options.userPassword) {
      throw new LdapAuthenticationError('userPassword must be provided');
    }
    if (options.adminDn) {
      if (!options.adminPassword) {
        throw new LdapAuthenticationError(
          'adminDn and adminPassword must be both provided.',
        );
      }
      return await this.authenticateWithAdmin(
        options.adminDn,
        options.adminPassword,
        options.userSearchBase,
        options.usernameAttribute,
        options.username,
        options.userPassword,
        options.starttls,
        options.ldapOpts,
        options.groupsSearchBase,
        options.groupClass,
        options.groupMemberAttribute,
        options.groupMemberUserAttribute,
        options.attributes,
      );
    }
    if (!options.userDn) {
      throw new LdapAuthenticationError(
        'adminDn/adminPassword OR userDn must be provided',
      );
    }
    return await this.authenticateWithUser(
      options.userDn,
      options.userSearchBase,
      options.usernameAttribute,
      options.username,
      options.userPassword,
      options.starttls,
      options.ldapOpts,
      options.groupsSearchBase,
      options.groupClass,
      options.groupMemberAttribute,
      options.groupMemberUserAttribute,
      options.attributes,
    );
  }

  async loginViaLDAP(
    ldapClient: string,
    username: string,
    userPassword: string,
  ) {
    const client = await this.ldap.findOne({ uuid: ldapClient });
    if (!client) {
      throw new BadRequestException({ InvalidLDAPClient: ldapClient });
    }
    await this.authenticate({
      username,
      ldapOpts: { url: client.url },
      adminDn: client.adminDn,
      adminPassword: client.adminPassword,
      userSearchBase: client.userSearchBase,
      usernameAttribute: client.usernameAttribute,
      verifyUserExists: true,
    });

    const ldapUser = (await this.authenticate({
      username,
      userPassword,
      ldapOpts: { url: client.url },
      adminDn: client.adminDn,
      adminPassword: client.adminPassword,
      userSearchBase: client.userSearchBase,
      usernameAttribute: client.usernameAttribute,
    })) as LDAPUser;
    const localUser = await this.getLocalUser(ldapUser, client);
    if (localUser) {
      return localUser;
    }
    return this.createUser(ldapUser, client);
  }

  createUser(ldapUser: LDAPUser, client: LDAPClient) {
    const user = {
      email: ldapUser[client.emailAttribute],
      phone: ldapUser[client.phoneAttribute],
      name: ldapUser[client.fullNameAttribute],
      isEmailVerified: true,
      uuid: uuidv4(),
    } as User;
    user.email = user.email?.trim().toLowerCase();

    this.apply(new UserAccountAddedEvent(user));
    return user;
  }

  async getLocalUser(ldapUser: LDAPUser, client: LDAPClient) {
    const email = ldapUser[client.emailAttribute];
    const phone = ldapUser[client.phoneAttribute];
    if (!phone && !email) {
      throw new BadRequestException({ MissingPhoneOrEmail: { phone, email } });
    }
    const userEmail = await this.userManager.findByEmail(
      (email as string).trim().toLowerCase(),
    );
    const userPhone = await this.userManager.findByPhone(phone as string);

    if (userEmail && userPhone) {
      // Found phone and email locally

      if (userEmail.uuid === userPhone.uuid) {
        // phone and email belong to same user

        return userEmail;
      }

      if (userEmail.uuid !== userPhone.uuid) {
        // phone and email do not belong to same user
        // set priority to email (TODO: check from server)

        return userEmail;
      }
    } else if (userEmail) {
      // Select email if only email found

      return userEmail;
    } else if (userPhone) {
      // Select phone if only phone found

      return userPhone;
    }
  }

  async removeLDAPClient(uuid: string, userUuid: string) {
    const ldapClient = await this.ldap.findOne({ uuid });
    if (ldapClient) {
      const kerberosRealm = await this.kerberosRealm.findOne({
        ldapClient: ldapClient.uuid,
      });
      if (kerberosRealm) {
        throw new BadRequestException({
          KerberosRealm: {
            uuid: kerberosRealm.uuid,
            ldapClient: kerberosRealm.ldapClient,
          },
        });
      }
      this.apply(new LDAPClientRemovedEvent(userUuid, ldapClient));
    } else {
      throw new NotFoundException({ uuid });
    }
  }

  async addLDAPClient(payload: CreateLDAPClientDto, createdBy: string) {
    const params = {
      ...payload,
      ...{
        createdBy,
        creation: new Date(),
      },
    } as LDAPClient;
    this.apply(new LDAPClientAddedEvent(params));
    return params;
  }

  async modifyLDAPClient(payload: CreateLDAPClientDto, uuid: string) {
    const ldapClient = await this.ldap.findOne({ uuid });
    Object.assign(ldapClient, payload);
    ldapClient.modified = new Date();
    this.apply(new LDAPClientModifiedEvent(ldapClient));
    return ldapClient;
  }

  async findOne(params) {
    return await this.ldap.findOne(params);
  }
}
