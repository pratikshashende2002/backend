import { IsNotEmpty, IsOptional, IsUUID } from 'class-validator';

export class CreateKerberosRealmDto {
  @IsNotEmpty()
  name: string;

  @IsOptional()
  description: string;

  @IsNotEmpty()
  domain?: string;

  @IsUUID()
  ldapClient: string;

  @IsOptional()
  servicePrincipalName: string;
}
