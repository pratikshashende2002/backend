import { IsNotEmpty, IsOptional, IsUUID } from 'class-validator';

export class CreateLDAPClientDto {
  @IsNotEmpty()
  name: string;

  @IsOptional()
  description: string;

  @IsNotEmpty()
  url?: string;

  @IsNotEmpty()
  adminDn?: string;

  @IsNotEmpty()
  adminPassword?: string;

  @IsNotEmpty()
  userSearchBase?: string;

  @IsNotEmpty()
  usernameAttribute?: string;

  @IsNotEmpty()
  emailAttribute?: string;

  @IsNotEmpty()
  phoneAttribute?: string;

  @IsNotEmpty()
  fullNameAttribute?: string;

  @IsUUID()
  @IsOptional()
  clientId: string;
}
