import { IsNotEmpty, IsUrl, IsOptional } from 'class-validator';

export class CreateSocialLoginDto {
  @IsNotEmpty()
  name: string;

  @IsOptional()
  description: string;

  @IsOptional()
  clientId: string;

  @IsOptional()
  clientSecret: string;

  @IsUrl({ require_tld: false })
  authorizationURL: string;

  @IsUrl({ require_tld: false })
  tokenURL: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  introspectionURL: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  baseURL: string;

  @IsUrl({ require_tld: false })
  profileURL: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  revocationURL: string;

  @IsNotEmpty({ each: true })
  scope: string[];
}
