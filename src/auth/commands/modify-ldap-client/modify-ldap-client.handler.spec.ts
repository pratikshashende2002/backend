import { Test } from '@nestjs/testing';
import { CommandBus, CqrsModule, EventPublisher } from '@nestjs/cqrs';
import { LDAPClientAggregateService } from '../../aggregates/ldap-aggregate/ldap-client-aggregate.service';
import { ModifyLDAPClientHandler } from './modify-ldap-client.handler';
import { ModifyLDAPClientCommand } from './modify-ldap-client.command';
import { CreateLDAPClientDto } from '../../controllers/ldap-client/ldap-client-create.dto';
import { LDAPClient } from '../../entities/ldap-client/ldap-client.interface';

describe('Command: ModifyLDAPClientHandler', () => {
  let commandBus$: CommandBus;
  let manager: LDAPClientAggregateService;
  let commandHandler: ModifyLDAPClientHandler;
  let publisher: EventPublisher;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        ModifyLDAPClientHandler,
        {
          provide: CommandBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: LDAPClientAggregateService,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    commandBus$ = module.get<CommandBus>(CommandBus);
    manager = module.get<LDAPClientAggregateService>(
      LDAPClientAggregateService,
    );
    commandHandler = module.get<ModifyLDAPClientHandler>(
      ModifyLDAPClientHandler,
    );
    publisher = module.get<EventPublisher>(EventPublisher);
  });

  it('should be defined', () => {
    expect(commandBus$).toBeDefined();
    expect(manager).toBeDefined();
    expect(commandHandler).toBeDefined();
  });

  it('should remove client using the LDAPClientAggregateService', async () => {
    const modifyLDAPClient = jest.fn(() =>
      Promise.resolve({} as LDAPClient & { _id: any }),
    );
    manager.modifyLDAPClient = modifyLDAPClient;
    commandBus$.execute = jest.fn();
    publisher.mergeObjectContext = jest.fn().mockImplementation((...args) => ({
      commit: () => {},
      modifyLDAPClient,
    }));
    await commandHandler.execute(
      new ModifyLDAPClientCommand({} as CreateLDAPClientDto, '420'),
    );
    expect(manager.modifyLDAPClient).toHaveBeenCalledTimes(1);
  });
});
