import { Test } from '@nestjs/testing';
import { CommandBus, CqrsModule, EventPublisher } from '@nestjs/cqrs';
import { AddLDAPClientHandler } from './add-ldap-client.handler';
import { AddLDAPClientCommand } from './add-ldap-client.command';
import { CreateLDAPClientDto } from '../../controllers/ldap-client/ldap-client-create.dto';
import { LDAPClient } from '../../entities/ldap-client/ldap-client.interface';
import { LDAPClientAggregateService } from '../../aggregates/ldap-aggregate/ldap-client-aggregate.service';

describe('Command: AddLDAPClientHandler', () => {
  let commandBus$: CommandBus;
  let manager: LDAPClientAggregateService;
  let commandHandler: AddLDAPClientHandler;
  let publisher: EventPublisher;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        AddLDAPClientHandler,
        {
          provide: CommandBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: LDAPClientAggregateService,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    commandBus$ = module.get<CommandBus>(CommandBus);
    manager = module.get<LDAPClientAggregateService>(
      LDAPClientAggregateService,
    );
    commandHandler = module.get<AddLDAPClientHandler>(AddLDAPClientHandler);
    publisher = module.get<EventPublisher>(EventPublisher);
  });

  it('should be defined', () => {
    expect(commandBus$).toBeDefined();
    expect(manager).toBeDefined();
    expect(commandHandler).toBeDefined();
  });

  it('should remove role using the LDAPClientAggregateService', async () => {
    const addLDAPClient = jest.fn(() => Promise.resolve({} as LDAPClient));
    manager.addLDAPClient = addLDAPClient;
    commandBus$.execute = jest.fn();
    publisher.mergeObjectContext = jest.fn().mockImplementation((...args) => ({
      commit: () => {},
      addLDAPClient,
    }));
    await commandHandler.execute(
      new AddLDAPClientCommand({} as CreateLDAPClientDto, '420'),
    );
    expect(manager.addLDAPClient).toHaveBeenCalledTimes(1);
  });
});
