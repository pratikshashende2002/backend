import { ICommand } from '@nestjs/cqrs';
import { CreateKerberosRealmDto } from '../../controllers/kerberos/kerberos-realm-create.dto';

export class ModifyKerberosRealmCommand implements ICommand {
  constructor(
    public readonly payload: CreateKerberosRealmDto,
    public readonly uuid: string,
  ) {}
}
