import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveLDAPClientCommand } from './remove-ldap-client.command';
import { LDAPClientAggregateService } from '../../aggregates/ldap-aggregate/ldap-client-aggregate.service';

@CommandHandler(RemoveLDAPClientCommand)
export class RemoveLDAPClientHandler
  implements ICommandHandler<RemoveLDAPClientCommand>
{
  constructor(
    private readonly manager: LDAPClientAggregateService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: RemoveLDAPClientCommand) {
    const { userUuid, ldapClientUuid } = command;

    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.removeLDAPClient(ldapClientUuid, userUuid);
    aggregate.commit();
  }
}
