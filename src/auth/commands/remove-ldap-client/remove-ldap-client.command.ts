import { ICommand } from '@nestjs/cqrs';

export class RemoveLDAPClientCommand implements ICommand {
  constructor(
    public readonly userUuid: string,
    public readonly ldapClientUuid: string,
  ) {}
}
