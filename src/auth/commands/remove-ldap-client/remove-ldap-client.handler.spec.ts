import { Test } from '@nestjs/testing';
import { CommandBus, CqrsModule, EventPublisher } from '@nestjs/cqrs';
import { RemoveLDAPClientHandler } from './remove-ldap-client.handler';
import { RemoveLDAPClientCommand } from './remove-ldap-client.command';
import { LDAPClientAggregateService } from '../../aggregates/ldap-aggregate/ldap-client-aggregate.service';
import { LDAPClient } from '../../entities/ldap-client/ldap-client.interface';

describe('Command: RemoveLDAPClientHandler', () => {
  let commandBus$: CommandBus;
  let manager: LDAPClientAggregateService;
  let commandHandler: RemoveLDAPClientHandler;
  let publisher: EventPublisher;

  const mockSocialLogin = {
    createdBy: '62843fea-1e30-4b1f-af91-3ff582ff9948',
    creation: new Date(),
    uuid: 'f97cef1e-4b15-4194-b40d-7ea0c0d8fa24',
  } as LDAPClient;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        RemoveLDAPClientHandler,
        {
          provide: CommandBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: LDAPClientAggregateService,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    commandBus$ = module.get<CommandBus>(CommandBus);
    manager = module.get<LDAPClientAggregateService>(
      LDAPClientAggregateService,
    );
    commandHandler = module.get<RemoveLDAPClientHandler>(
      RemoveLDAPClientHandler,
    );
    publisher = module.get<EventPublisher>(EventPublisher);
  });

  it('should be defined', () => {
    expect(commandBus$).toBeDefined();
    expect(manager).toBeDefined();
    expect(commandHandler).toBeDefined();
  });

  it('should remove LDAPClient using the LDAPClientAggregateService', async () => {
    manager.removeLDAPClient = jest.fn(() => Promise.resolve());
    publisher.mergeObjectContext = jest
      .fn()
      .mockImplementation((...args) => ({ commit: () => {} }));
    await commandHandler.execute(
      new RemoveLDAPClientCommand(
        mockSocialLogin.createdBy,
        mockSocialLogin.uuid,
      ),
    );
    expect(manager.removeLDAPClient).toHaveBeenCalledTimes(1);
  });
});
