import { Test } from '@nestjs/testing';
import { CommandBus, CqrsModule, EventPublisher } from '@nestjs/cqrs';
import { AddKerberosRealmHandler } from './add-kerberos-realm.handler';
import { AddKerberosRealmCommand } from './add-kerberos-realm.command';
import { CreateKerberosRealmDto } from '../../controllers/kerberos/kerberos-realm-create.dto';
import { KerberosRealm } from '../../entities/kerberos-realm/kerberos-realm.interface';
import { KerberosAuthAggregateService } from '../../aggregates/kerberos-auth-aggregate/kerberos-auth-aggregate.service';

describe('Command: AddKerberosRealmHandler', () => {
  let commandBus$: CommandBus;
  let manager: KerberosAuthAggregateService;
  let commandHandler: AddKerberosRealmHandler;
  let publisher: EventPublisher;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        AddKerberosRealmHandler,
        {
          provide: CommandBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: KerberosAuthAggregateService,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    commandBus$ = module.get<CommandBus>(CommandBus);
    manager = module.get<KerberosAuthAggregateService>(
      KerberosAuthAggregateService,
    );
    commandHandler = module.get<AddKerberosRealmHandler>(
      AddKerberosRealmHandler,
    );
    publisher = module.get<EventPublisher>(EventPublisher);
  });

  it('should be defined', () => {
    expect(commandBus$).toBeDefined();
    expect(manager).toBeDefined();
    expect(commandHandler).toBeDefined();
  });

  it('should add realm using the KerberosAuthAggregateService', async () => {
    const addKerberosRealm = jest.fn(() =>
      Promise.resolve({} as KerberosRealm),
    );
    manager.addKerberosRealm = addKerberosRealm;
    commandBus$.execute = jest.fn();
    publisher.mergeObjectContext = jest.fn().mockImplementation((...args) => ({
      commit: () => {},
      addKerberosRealm,
    }));
    await commandHandler.execute(
      new AddKerberosRealmCommand({} as CreateKerberosRealmDto, '420'),
    );
    expect(manager.addKerberosRealm).toHaveBeenCalledTimes(1);
  });
});
