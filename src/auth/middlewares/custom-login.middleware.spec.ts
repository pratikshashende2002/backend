import { TestingModule, Test } from '@nestjs/testing';
import { CustomLoginMiddleware } from './custom-login.middleware';
import { ClientService } from '../../client-management/entities/client/client.service';
import { ServerSettingsService } from '../../system-settings/entities/server-settings/server-settings.service';

describe('CustomLoginMiddleware', () => {
  let middleware: CustomLoginMiddleware;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CustomLoginMiddleware,
        {
          provide: ServerSettingsService,
          useFactory: () => jest.fn(),
        },
        {
          provide: ClientService,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    middleware = module.get<CustomLoginMiddleware>(CustomLoginMiddleware);
  });

  it('should be defined', () => {
    expect(middleware).toBeDefined();
  });
});
