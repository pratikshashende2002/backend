import { Request } from 'express';
import { Strategy } from 'passport';
import { NEGOTIATE } from '../../../constants/app-strings';

export interface KerberosStrategyOptions {
  /**
   * A string containing the service principal in the form 'type@fqdn' (e.g. 'imap@mail.apple.com').
   */
  servicePrincipalName?: string;
  passReqToCallback?: boolean;
  noUserRedirectUrl?: string;
  verbose?: boolean;
}

export class PassportKerberosNegotiationStrategy extends Strategy {
  private passReqToCallback: boolean;
  private verify: (...args: unknown[]) => unknown;
  name = 'krb5-negotiate';

  public constructor(
    readonly options: KerberosStrategyOptions,
    verify: (...args: unknown[]) => void,
  ) {
    super();
    if (!verify) {
      throw new TypeError(
        'KerberosNegotiationStrategy requires a verify callback',
      );
    }
    if (!options) {
      throw new Error('Kerberos authentication strategy requires options');
    }
    Strategy.call(this);
    this.verify = verify;
    this.passReqToCallback = options.passReqToCallback;
  }

  public async authenticate(
    req: Request & { user?: unknown },
    options: KerberosStrategyOptions,
  ): Promise<unknown> {
    const auth: string = req.headers.authorization;

    if (!auth) {
      return this.fail({ message: NEGOTIATE }, 401);
    }

    if (auth.lastIndexOf(`${NEGOTIATE} `, 0) !== 0) {
      this.error(new Error('Malformed authentication token: ' + auth));
      return;
    }

    const self = this;

    const verified = (err: Error, user: unknown, info) => {
      if (err) {
        self.error(err);
      }
      if (!user) {
        if (options.noUserRedirectUrl) {
          self.redirect(options.noUserRedirectUrl);
        }
        self.fail('No user found in the Kerberos database', 404);
      }
      self.success(user, info);
    };

    if (this.verify) {
      if (this.passReqToCallback) {
        void this.verify(req, auth, verified);
      } else {
        void this.verify(auth, verified);
      }
    }
  }
}
