import { Test, TestingModule } from '@nestjs/testing';
import { LDAPClientService } from './ldap-client.service';
import { LDAP_CLIENT } from './ldap-client.schema';

describe('LDAPServerService', () => {
  let service: LDAPClientService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        LDAPClientService,
        {
          provide: LDAP_CLIENT,
          useValue: {}, // provide mock values
        },
      ],
    }).compile();
    service = module.get<LDAPClientService>(LDAPClientService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
