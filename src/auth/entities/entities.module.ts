import { Module } from '@nestjs/common';
import { AuthorizationCodeService } from './authorization-code/authorization-code.service';
import { BearerTokenService } from './bearer-token/bearer-token.service';
import { OIDCKeyService } from './oidc-key/oidc-key.service';
import { SocialLoginService } from './social-login/social-login.service';
import { SessionService } from './session/session.service';
import { AuthModuleEntities } from './entities';
import { UserClaimService } from './user-claim/user-claim.service';
import { LDAPClientService } from './ldap-client/ldap-client.service';
import { KerberosRealmService } from './kerberos-realm/kerberos-realm.service';

@Module({
  providers: [
    ...AuthModuleEntities,
    AuthorizationCodeService,
    BearerTokenService,
    OIDCKeyService,
    SocialLoginService,
    SessionService,
    UserClaimService,
    LDAPClientService,
    KerberosRealmService,
  ],
  exports: [
    ...AuthModuleEntities,
    AuthorizationCodeService,
    BearerTokenService,
    OIDCKeyService,
    SocialLoginService,
    SessionService,
    UserClaimService,
    LDAPClientService,
    KerberosRealmService,
  ],
})
export class AuthEntitiesModule {}
