import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

export const schema = new mongoose.Schema(
  {
    name: String,
    description: String,
    uuid: { type: String, default: uuidv4 },
    domain: String,
    ldapClient: String,
    servicePrincipalName: String,
    createdBy: String,
    modifiedBy: String,
    creation: { type: Date, default: () => new Date() },
    modified: Date,
  },
  { collection: 'kerberos_realm', versionKey: false },
);

export const KerberosRealm = schema;

export const KERBEROS_REALM = 'KerberosRealm';

export const KerberosRealmModel = mongoose.model(KERBEROS_REALM, KerberosRealm);
