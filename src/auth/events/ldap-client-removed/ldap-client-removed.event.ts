import { IEvent } from '@nestjs/cqrs';
import { LDAPClient } from '../../entities/ldap-client/ldap-client.interface';

export class LDAPClientRemovedEvent implements IEvent {
  constructor(
    public readonly userUuid: string,
    public readonly ldapClient: LDAPClient,
  ) {}
}
