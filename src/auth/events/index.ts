import { SocialLoginRemovedHandler } from './social-login-removed/social-login-removed.handler';
import { BearerTokenRemovedHandler } from './bearer-token-removed/bearer-token-removed.handler';
import { UserSignedUpViaSocialLoginHandler } from './social-login-user-signed-up/social-login-user-signed-up.handler';
import { SocialLoginAddedHandler } from './social-login-added/social-login-added.handler';
import { SocialLoginModifiedHandler } from './social-login-modified/social-login-modified.handler';
import { WebAuthnKeyChallengeRequestedHandler } from './webauthn-key-registration-requested/webauthn-key-challenge-requested.handler';
import { WebAuthnKeyRegisteredHandler } from './webauthn-key-registered/webauthn-key-registered.handler';
import { UserLoggedInWithWebAuthnHandler } from './user-logged-in-with-webauthn-key/user-logged-in-with-webauthn-key.handler';
import { UserAuthenticatorRemovedHandler } from './user-authenticator-removed/user-authenticator-removed.handler';
import { UserAuthenticatorModifiedEvent } from './user-authenticator-modified/user-authenticator-modified.event';
import { UserLogInHOTPGeneratedHandler } from './user-login-hotp-generated/user-login-hotp-generated.handler';
import { UnverifiedPhoneAddedHandler } from './unverified-phone-added/unverified-phone-added.handler';
import { PhoneVerifiedHandler } from './phone-verified/phone-verified.handler';
import { BearerTokenAddedHandler } from './bearer-token-added/bearer-token-added.handler';
import { UnverifiedEmailAddedHandler } from './unverified-email-added/unverified-email-added.handler';
import { LDAPClientAddedHandler } from './ldap-client-added/ldap-client-added.handler';
import { LDAPClientModifiedHandler } from './ldap-client-modified/ldap-client-modified.handler';
import { LDAPClientRemovedHandler } from './ldap-client-removed/ldap-client-removed.handler';
import { KerberosRealmAddedHandler } from './kerberos-realm-added/kerberos-realm-added.handler';
import { KerberosRealmModifiedHandler } from './kerberos-realm-modified/kerberos-realm-modified.handler';
import { KerberosRealmRemovedHandler } from './kerberos-realm-removed/kerberos-realm-removed.handler';

export { SocialLoginUserSignedUpEvent } from './social-login-user-signed-up/social-login-user-signed-up.event';

export const AuthEventHandlers = [
  SocialLoginRemovedHandler,
  BearerTokenRemovedHandler,
  UserSignedUpViaSocialLoginHandler,
  SocialLoginAddedHandler,
  SocialLoginModifiedHandler,
  WebAuthnKeyChallengeRequestedHandler,
  WebAuthnKeyRegisteredHandler,
  UserLoggedInWithWebAuthnHandler,
  UserAuthenticatorRemovedHandler,
  UserAuthenticatorModifiedEvent,
  UserLogInHOTPGeneratedHandler,
  UnverifiedPhoneAddedHandler,
  PhoneVerifiedHandler,
  BearerTokenAddedHandler,
  UnverifiedEmailAddedHandler,
  LDAPClientAddedHandler,
  LDAPClientModifiedHandler,
  LDAPClientRemovedHandler,
  KerberosRealmAddedHandler,
  KerberosRealmModifiedHandler,
  KerberosRealmRemovedHandler,
];
