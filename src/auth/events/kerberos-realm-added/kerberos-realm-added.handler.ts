import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { KerberosRealmAddedEvent } from './kerberos-realm-added.event';
import { KerberosRealmService } from '../../entities/kerberos-realm/kerberos-realm.service';

@EventsHandler(KerberosRealmAddedEvent)
export class KerberosRealmAddedHandler
  implements IEventHandler<KerberosRealmAddedEvent>
{
  constructor(private readonly realm: KerberosRealmService) {}
  handle(event: KerberosRealmAddedEvent) {
    this.realm
      .save(event.realm)
      .then(added => {})
      .catch(error => {});
  }
}
