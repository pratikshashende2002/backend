import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { LDAPClientModifiedEvent } from './ldap-client-modified.event';
import { LDAPClientService } from '../../entities/ldap-client/ldap-client.service';

@EventsHandler(LDAPClientModifiedEvent)
export class LDAPClientModifiedHandler
  implements IEventHandler<LDAPClientModifiedEvent>
{
  constructor(private readonly ldapClient: LDAPClientService) {}
  handle(event: LDAPClientModifiedEvent) {
    this.ldapClient
      .save(event.ldapClient)
      .then(modified => {})
      .catch(error => {});
  }
}
