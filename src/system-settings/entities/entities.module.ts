import { Module } from '@nestjs/common';
import { ServerSettingsService } from './server-settings/server-settings.service';
import { SystemSettingsModuleEntities } from './entities';
import { QueueLogService } from './queue-log/queue-log.service';

@Module({
  providers: [
    ...SystemSettingsModuleEntities,
    ServerSettingsService,
    QueueLogService,
  ],
  exports: [
    ...SystemSettingsModuleEntities,
    ServerSettingsService,
    QueueLogService,
  ],
})
export class SystemSettingsEntitiesModule {}
