import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { QUEUE_LOG } from './queue-log.schema';
import { QueueLog } from './queue-log.interface';

@Injectable()
export class QueueLogService {
  constructor(
    @Inject(QUEUE_LOG)
    private readonly model: Model<QueueLog>,
  ) {}

  async save(params) {
    const doc = new this.model(params);
    return await doc.save();
  }

  async update(doc: QueueLog) {
    return await doc.save();
  }

  async find(params?): Promise<QueueLog[]> {
    return await this.model.find(params);
  }

  async findOne(params) {
    return await this.model.findOne(params).exec();
  }
}
