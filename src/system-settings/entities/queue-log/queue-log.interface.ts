import { Document } from 'mongoose';

export class QueueLog extends Document {
  uuid?: string;
  data?: any | string;
  senderType?: string;
  senderUuid?: string;
}
