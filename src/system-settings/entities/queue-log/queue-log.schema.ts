import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

export const QUEUE_LOG_COLLECTION = 'queue_log';

export const QueueLog = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4 },
    data: mongoose.Schema.Types.Mixed,
    senderType: String,
    senderUuid: String,
  },
  { collection: QUEUE_LOG_COLLECTION, versionKey: false, strict: false },
);

export const QUEUE_LOG = 'QueueLog';

export const QueueLogModel = mongoose.model(QUEUE_LOG, QueueLog);
