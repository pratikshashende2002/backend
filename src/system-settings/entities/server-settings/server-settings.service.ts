import { Injectable, HttpStatus, HttpException, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { settingsNotFoundException } from '../../../common/filters/exceptions';
import { SERVER_SETTINGS } from './server-settings.schema';
import { ServerSettings } from './server-settings.interface';
import { i18n } from '../../../i18n/i18n.config';
import { SERVICE } from '../../../constants/app-strings';

@Injectable()
export class ServerSettingsService {
  constructor(
    @Inject(SERVER_SETTINGS)
    private readonly settingsModel: Model<ServerSettings>,
  ) {}

  async save(params) {
    const settings = await this.findWithoutError();
    if (settings) {
      throw new HttpException(
        i18n.__('Setup already complete'),
        HttpStatus.UNAUTHORIZED,
      );
    }
    const createdSettings = new this.settingsModel(params);
    return await createdSettings.save();
  }

  async update(settings: ServerSettings) {
    return await settings.save();
  }

  async find(): Promise<ServerSettings> {
    const settings = await this.findWithoutError();
    if (!settings) {
      throw settingsNotFoundException;
    }
    return settings;
  }

  async findWithoutError() {
    return await this.findOne({ service: SERVICE });
  }

  async findOne(params) {
    return await this.settingsModel.findOne(params).exec();
  }
}
