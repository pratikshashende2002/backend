import {
  IsUrl,
  IsOptional,
  IsUUID,
  IsBoolean,
  IsNumber,
  Min,
  IsString,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { THIRTY_NUMBER, TEN_NUMBER } from '../../../constants/app-strings';

export class ServerSettingDto {
  @IsUrl({ require_tld: false })
  @ApiProperty({
    description: 'The URL of the server.',
    type: 'string',
    required: true,
  })
  issuerUrl: string;

  @IsOptional()
  @IsUUID()
  @ApiProperty({
    description: 'OAuth 2.0 Client ID for Infrastructure Console',
    type: 'string',
  })
  infrastructureConsoleClientId?: string;

  @IsOptional()
  @IsUUID()
  @ApiProperty({
    description: 'Cloud Storage Bucket',
    type: 'string',
  })
  backupBucket?: string;

  @IsOptional()
  @IsBoolean()
  disableSignup?: boolean;

  @IsOptional()
  @IsNumber()
  otpExpiry?: number;

  @IsOptional()
  @IsBoolean()
  enableChoosingAccount?: boolean;

  @IsOptional()
  @IsNumber()
  @Min(THIRTY_NUMBER)
  refreshTokenExpiresInDays?: number;

  @IsOptional()
  @IsNumber()
  @Min(TEN_NUMBER)
  authCodeExpiresInMinutes?: number;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'The name of host organization.',
    type: 'string',
  })
  organizationName: string;

  @IsBoolean()
  @IsOptional()
  @ApiProperty({
    description: 'Allow client to show custom login page',
    type: 'string',
  })
  enableCustomLogin: boolean;

  @IsBoolean()
  @IsOptional()
  @ApiProperty({
    description: 'Allow users to register phone',
    type: 'boolean',
  })
  enableUserPhone: boolean;

  @IsBoolean()
  @IsOptional()
  @ApiProperty({
    description: 'Disallow user to be deleted',
    type: 'boolean',
  })
  isUserDeleteDisabled: boolean;

  @IsUUID()
  @IsOptional()
  systemEmailAccount: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  logoUrl?: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  faviconUrl?: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  privacyUrl?: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  helpUrl?: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  termsUrl?: string;

  @IsOptional()
  copyrightMessage?: string;

  @IsOptional()
  @IsUUID()
  @ApiProperty({
    description: 'Cloud Storage Bucket for avatars',
    type: 'string',
  })
  avatarBucket?: string;
}
