import { Document } from 'mongoose';

export interface ServerSettings extends Document {
  uuid?: string;
  issuerUrl?: string;
  infrastructureConsoleClientId?: string;
  backupBucket?: string;
  disableSignup?: boolean;
  otpExpiry?: number;
  enableChoosingAccount?: boolean;
  enableCustomLogin?: boolean;
  refreshTokenExpiresInDays?: number;
  authCodeExpiresInMinutes?: number;
  organizationName?: string;
  enableUserPhone?: boolean;
  isUserDeleteDisabled?: boolean;
  service?: string;
  systemEmailAccount?: string;
  logoUrl?: string;
  faviconUrl?: string;
  privacyUrl?: string;
  helpUrl?: string;
  termsUrl?: string;
  copyrightMessage?: string;
  avatarBucket?: string;
}
