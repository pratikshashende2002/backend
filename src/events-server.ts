import { INestApplication, Logger } from '@nestjs/common';
import { MicroserviceOptions } from '@nestjs/microservices';
import { ConfigService } from './config/config.service';
import { useEventClientFactory } from './common/events-microservice.client';

const LISTENING_TO_EVENTS = 'Listening to Events';

export const RETRY_ATTEMPTS = 3;
export const RETRY_DELAY = 10;

export function setupEvents(app: INestApplication) {
  const config = app.get<ConfigService>(ConfigService);
  const options = useEventClientFactory(config);
  const events = app.connectMicroservice<MicroserviceOptions>(options);
  events
    .listen()
    .then(() => Logger.log(LISTENING_TO_EVENTS, events.constructor.name));
}
