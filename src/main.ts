import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ExpressAdapter } from '@nestjs/platform-express';
import { ExpressServer } from './express-server';
import { ConfigService } from './config/config.service';
import { setupEvents } from './events-server';
import { API } from './constants/app-strings';
import {
  JWKS_ENDPOINT,
  OPENID_CONFIGURATION_ENDPOINT,
} from './constants/url-strings';

async function bootstrap() {
  const authServer = new ExpressServer(new ConfigService());
  authServer.setupSecurity();
  authServer.setupI18n();

  const app = await NestFactory.create(
    AppModule,
    new ExpressAdapter(authServer.server),
  );

  // Set api prefix
  app.setGlobalPrefix(API, {
    exclude: [OPENID_CONFIGURATION_ENDPOINT, JWKS_ENDPOINT],
  });

  // Enable CORS
  app.enableCors();

  // Setup Swagger
  ExpressServer.setupSwagger(app);

  // Handlebars View engine
  ExpressServer.setupViewEngine(app);

  // Setup Session
  authServer.setupSession(app);

  // Setup Events
  setupEvents(app);

  await app.listen(Number(process.env.PORT || '3000'));
}

bootstrap();
