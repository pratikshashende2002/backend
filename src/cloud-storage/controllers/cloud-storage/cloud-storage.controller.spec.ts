import { Test, TestingModule } from '@nestjs/testing';
import { CloudStorageController } from './cloud-storage.controller';
import { HttpService } from '@nestjs/axios';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { StorageService } from '../../../cloud-storage/entities/storage/storage.service';
import { RoleGuard } from '../../../auth/guards/role.guard';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { BearerTokenGuard } from '../../../auth/guards/bearer-token.guard';
import { BearerTokenService } from '../../../auth/entities/bearer-token/bearer-token.service';

describe('CloudStorage Controller', () => {
  let controller: CloudStorageController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CloudStorageController],
      providers: [
        {
          provide: CommandBus,
          useFactory: (...args) => jest.fn(),
        },
        {
          provide: QueryBus,
          useFactory: (...args) => jest.fn(),
        },
        {
          provide: StorageService,
          useFactory: (...args) => jest.fn(),
        },
        {
          provide: ServerSettingsService,
          useValue: {},
        },
        {
          provide: BearerTokenService,
          useValue: {},
        },
        {
          provide: HttpService,
          useFactory: (...args) => jest.fn(),
        },
      ],
    })
      .overrideGuard(BearerTokenGuard)
      .useValue({})
      .overrideGuard(RoleGuard)
      .useValue({})
      .compile();

    controller = module.get<CloudStorageController>(CloudStorageController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
