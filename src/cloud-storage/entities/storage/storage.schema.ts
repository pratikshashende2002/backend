import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

const schema = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4 },
    version: String,
    name: String,
    region: String,
    endpoint: String,
    accessKey: String,
    secretKey: String,
    bucket: String,
    basePath: String,
  },
  { collection: 'object_storage', versionKey: false },
);

export const Storage = schema;

export const STORAGE = 'Storage';

export const StorageModel = mongoose.model(STORAGE, Storage);
