import { IEvent } from '@nestjs/cqrs';
import { Storage } from '../../entities/storage/storage.interface';

export class FileUploadedCloudBucketEvent implements IEvent {
  constructor(
    public readonly clientUploadedFile: any,
    public readonly storageSettings: Storage,
    public readonly clientHttpReq: any,
    public readonly fileUploadedPermissions: string,
  ) {}
}
